void setup()
{
 size(1920, 1080, P3D);
 camera(0, 0, -(height / 2.0) / tan(PI * 30.0f /180.0f), 0, 0, 0, 0, -1, 0);
 background (0);
}

void draw()
{
  float gauss = randomGaussian();
  float standardDeviation = 500;
  float mean = 0;
  
  float xPos = (standardDeviation * gauss) + mean;
  float yPos = random(-500, 500);
  fill(random(255), random(255), random(255), 75);
  noStroke();
 
  circle(xPos, yPos, random(20,50));
  
  float gauss2 = randomGaussian();
  float standardDeviation2 = 300;
  float mean2 = 50;
  
  float xPos2 = (standardDeviation2 * gauss2) + mean2;
  float yPos2 = random(-500, 500);
  
  circle(xPos2, yPos2, random(50, 75));
  
  float gauss3 = randomGaussian();
  float standardDeviation3 = 400;
  float mean3 = -50;
  
  float xPos3 = (standardDeviation3 * gauss3) + mean3;
  float yPos3 = random(-500, 500);
  circle(xPos3, yPos3, random(75, 100));
  
  if (frameCount == 1000)
  {
    background(0);
  }
}
